#!/usr/bin/env ruby
require 'readline'
require 'csv'
require "prawn"

class MovieAggregator

	def initialize
		@movie_list = []
		puts "Console app initiated!!!\n"
		common_interface
		loop do
			action = input("Select an action")
			break if action =~ /(?:ex|qu)it/i
			case action
			when "add"
				add_movie_details_interface
			when "export"
				export_interface 
			else
				puts "We have no idea what you're try to say!!!\n"
				puts "Please choose an action from the Menu\n"
				common_interface
			end
		end
	end
	private
	def input(prompt="", required = false, newline=false)
	  prompt += "\n" if newline
	  loop do
	  	input = Readline.readline(prompt + ": ", true).squeeze(" ").strip
	  	return input unless required && input.empty?
	  	puts prompt + " is required!\n"
	  end
	end

	def common_interface
		puts "=================================="
		puts "============== Menu =============="
		puts "1. Add"
		puts "2. Export"
		puts "3. Exit"
		puts "=================================="
	end

	def add_movie_details_interface(movie_list = [])
		puts "Please enter movie details and press exit any time \nto return to main menu"
		loop do
		  name = input("Movie name", true)
		  break if name =~ /(?:ex|qu)it/i
		  run_time = input "Runtime"
		  break if run_time =~ /(?:ex|qu)it/i
		  language = input "Language"
		  break if language =~ /(?:ex|qu)it/i
		  lead_actor = input "Lead actor"
		  break if lead_actor =~ /(?:ex|qu)it/i
		  genre = input "Genre"
		  break if genre =~ /(?:ex|qu)it/i
		  status = input "Want to save the data (Y/N)"
		  @movie_list << { 
				:name => name,
				:run_time => run_time,
				:language => language,
				:lead_actor => lead_actor,
				:genre => genre
			} if status =~ /yes|y/i
			puts @movie_list.inspect
		  continue = input("Continue adding moving details ? (Y/N)")
		  break unless continue =~ /yes|y/i
		end
		return movie_list
	end
	
	def export_interface
		if @movie_list.length > 0
			# supported_formats = Export.new.public_methods(false)
			puts "Supported formats: txt, csv & pdf\n"
			filename = input("Filename")
			return if filename =~ /(?:ex|qu)it/i
			format = input("Format")
			return if format =~ /(?:ex|qu)it/i

			export = Export.new(@movie_list, filename)
			export.method(format).call
		else
			puts "No data to export :("
		end
 	rescue NameError => e
    puts "'#{format}' file format is not supported at this moment!"
  rescue Exception => e
    puts "'#{format}' file format is not supported at this moment!"
	end

	def init
		puts "Console app initiated!!!\n"
		common_interface
		loop do
			action = input("Select an action")
			break if action =~ /(?:ex|qu)it/i
			case action
			when "add"
				add_movie_details_interface
			when "export"
				export_interface 
			else
				puts "We have no idea what you're trying to say!!!\n"
				puts "Please choose an action from the Menu\n"
				common_interface
			end
		end
	end
end


class Export
	def initialize(data, name)
		@data = data
		@name = name || 'file'
	end

	private
	def pdf
		Prawn::Document.generate(@name + ".pdf") do |pdf|
		 pdf.text  ["Name", "Runtime", "Language", "Lead actor", "Genre"].join("\t|\t")
		 @data.each do |line|
			  pdf.text [line[:name], line[:run_time], line[:language], line[:lead_actor], line[:genre]].join("\t|\t")
			end
		end
		puts 'data saved!'
	end

	def csv
		CSV.open(@name + ".csv", "wb") do |csv|
			csv << ["Name", "Runtime", "Language", "Lead actor", "Genre"]
			@data.each do |line|
			  csv << [line[:name], line[:run_time], line[:language], line[:lead_actor], line[:genre]]
			end
		end
		puts 'data saved!'
	end

	def txt
		File.open(@name + ".txt", "w+") do |file|
			file.puts ["Name", "Runtime", "Language", "Lead actor", "Genre"].join("\t|\t")
			@data.each do |line|
			  file.puts [line[:name], line[:run_time], line[:language], line[:lead_actor], line[:genre]].join("\t|\t")
			end
		end
		puts 'data saved!'
	end
end

movie = MovieAggregator.new


